#ifndef VECTOR3_HASH_H
#define VECTOR3_HASH_H

#include "Vector3.h"

#define HASH_ARRAY_SIZE 1024

typedef struct DataItem
{
    Vector3 *key;
    int data;

    //void (*Add)(struct DataItem *, Vector3 *, int);
    //void (*Remove)(struct DataItem **, DataItem *);
    //struct DataItem *(*GetItem)(struct DataItem **, Vector3 *);
    //void (*Print)(struct DataItem **);
} DataItem;

static DataItem *hashTableDummyItem;

DataItem *Vector3Hash_GetItem(DataItem **hashArray, Vector3 *key);
void Vector3Hash_Add(DataItem **hashArray, Vector3 *key, int data);
DataItem *Vector3Hash_Remove(DataItem **hashArray, DataItem *item);
void Vector3Hash_Print(DataItem **hashArray);

#endif