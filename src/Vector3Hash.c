#include "Vector3Hash.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

void CreateDummyItem()
{
    if (hashTableDummyItem != NULL)
        return;

    hashTableDummyItem = malloc(sizeof(*hashTableDummyItem));
    hashTableDummyItem->data = -1;
    hashTableDummyItem->key = GetMinusOneVector3();
}

DataItem *Vector3Hash_GetItem(DataItem **hashArray, Vector3 *key)
{
    int hashIndex = key->GetHashCode(key, HASH_ARRAY_SIZE);

    //move in array until an empty
    while (hashArray[hashIndex] != NULL)
    {
        if (key->Equals(key, hashArray[hashIndex]->key))
            return hashArray[hashIndex];

        //go to next cell
        ++hashIndex;

        //wrap around the table
        hashIndex %= HASH_ARRAY_SIZE;
    }

    return NULL;
}

void Vector3Hash_Add(DataItem **hashArray, Vector3 *key, int data)
{
    CreateDummyItem();

    DataItem *item = malloc(sizeof(*item));
    item->data = data;
    item->key = key;

    //get the hash
    //int hashIndex = HashCode(key, HASH_ARRAY_SIZE);
    int hashIndex = key->GetHashCode(key, HASH_ARRAY_SIZE);

    //move in array until an empty or deleted cell
    while (hashArray[hashIndex] != NULL && !hashArray[hashIndex]->key->Equals(hashArray[hashIndex]->key, hashTableDummyItem->key))
    {
        //go to next cell
        ++hashIndex;

        //wrap around the table
        hashIndex %= HASH_ARRAY_SIZE;
    }

    hashArray[hashIndex] = item;
}

DataItem *Vector3Hash_Remove(DataItem **hashArray, DataItem *item)
{
    CreateDummyItem();

    Vector3 *key = item->key;

    //get the hash
    //int hashIndex = HashCode(key, HASH_ARRAY_SIZE);
    int hashIndex = key->GetHashCode(key, HASH_ARRAY_SIZE);

    //move in array until an empty
    while (hashArray[hashIndex] != NULL)
    {
        if (key->Equals(key, hashArray[hashIndex]->key))
        {
            DataItem *temp = hashArray[hashIndex];

            //assign a dummy item at deleted position
            hashArray[hashIndex] = hashTableDummyItem;
            return temp;
        }

        //go to next cell
        ++hashIndex;

        //wrap around the table
        hashIndex %= HASH_ARRAY_SIZE;
    }

    return NULL;
}

void Vector3Hash_Print(DataItem **hashArray)
{
    int i = 0;

    for (i = 0; i < HASH_ARRAY_SIZE; i++)
    {
        if (hashArray[i] != NULL)
            printf("(%d,%d)\t", hashArray[i]->key, hashArray[i]->data);
        else
            printf("~~\t");
    }

    printf("\n");
}