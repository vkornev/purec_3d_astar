#ifndef ASTAR_H
#define ASTAR_H

#include "Vector3.h"
#include "Vector3List.h"
#include "PathNode.h"
#include "PathNodeList.h"
#include "Vector3Hash.h"

Vector3List *FindPath(DataItem **, Vector3 *, Vector3 *, Vector3 *);
static float Distance(Vector3 *, Vector3 *);
static int GetHeuristicPathLength(Vector3 *, Vector3 *);
static PathNode *GetBestNode(PathNodeList *);
static Vector3List *GetPathForNode(PathNode *);
static PathNodeList *GetNeighbours(DataItem **, Vector3 *, PathNode *, Vector3 *);

#endif