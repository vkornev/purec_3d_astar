#include "Vector3List.h"
#include <stdlib.h>

Vector3List *new_Vector3List(Vector3 *firstElement)
{
    Vector3List *head = malloc(sizeof(*head));
    head->data = firstElement;
    head->next = NULL;
    head->Add = &Vector3List_Add;
    head->Remove = &Vector3List_Remove;
    head->GetItem = &Vector3List_GetItem;
    head->GetLength = &Vector3List_GetLength;

    return head;
}

void delete_Vector3List(Vector3List **head)
{
    if (*head == NULL)
        return;

    while (*head != NULL)
    {
        delete_Vector3((*head)->data);
        Vector3List *current = *head;
        *head = (*head)->next;
        free(current);
    }
}

static void Vector3List_Add(Vector3List **head, Vector3 *v)
{
    //create new element
    Vector3List *link = new_Vector3List(v);
    //assing top element of the list to new element's next link
    link->next = *head;
    //make new element the head
    *head = link;
}

static void Vector3List_Remove(Vector3List **head, Vector3 *v)
{
    if (*head == NULL)
        return;

    Vector3List *current = *head;
    Vector3List *previous = NULL;

    while (!current->data->Equals(current->data, v))
    {
        if (current->next == NULL)
            return;

        //store reference to current link
        previous = current;
        //move to next link
        current = current->next;
    }

    //found a match, update the link
    if (current == *head)
        *head = (*head)->next;
    else
        previous->next = current->next;
}

static Vector3List *Vector3List_GetItem(Vector3List **head, Vector3 *v)
{
    if (*head == NULL)
        return NULL;

    Vector3List *current = *head;

    while (!current->data->Equals(current->data, v))
    {
        //if it is last node
        if (current->next == NULL)
            return NULL;

        current = current->next;
    }

    return current;
}

static int Vector3List_GetLength(Vector3List **head)
{
    int length = 0;
    Vector3List *current = *head;
    while (current != NULL)
    {
        length++;
        current = current->next;
    }

    return length;
}