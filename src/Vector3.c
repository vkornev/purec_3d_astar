#include "Vector3.h"
#include <stdlib.h>
#include <stdio.h>

Vector3 *new_Vector3(int x, int y, int z)
{
    Vector3 *result = malloc(sizeof(*result));
    result->GetHashCode = &Vector3_GetHashCode;
    result->GetStrLength = &Vector3_GetStrLength;
    result->Equals = &Vector3_Equals;
    result->ToString = &Vector3_ToString;
    result->x = x;
    result->y = y;
    result->z = z;

    return result;
}

void delete_Vector3(Vector3 *v)
{
    if (v == NULL)
        return;

    free(v);
    v = NULL;
}

Vector3 *GetMinusOneVector3()
{
    if (minusOneVector3 == NULL)
        minusOneVector3 = new_Vector3(-1, -1, -1);

    return minusOneVector3;
}

static bool Vector3_Equals(Vector3 *a, Vector3 *b)
{
    return a->x == b->x && a->y == b->y && a->z == b->z;
}

static int Vector3_GetHashCode(Vector3 *v, int hashSize)
{
    return ((v->x << 1) + (v->y << 2) + (v->z << 3)) % hashSize;
    //return this->x ^ (this->y << 2) ^ (this->z >> 2);
}

static int Vector3_NumberLength(int n)
{
    if (n == 0)
        return 1;

    int count = 0;

    if (n < 0)
    {
        count++;
        n = abs(n);
    }

    while (n > 0)
    {
        n = n / 10;
        count++;
    }

    return count;
}

static int Vector3_GetStrLength(Vector3 *v)
{
    int vLength = 6; // (, , )
    vLength += Vector3_NumberLength(v->x) + Vector3_NumberLength(v->y) + Vector3_NumberLength(v->z);

    return vLength;
}

static char *Vector3_ToString(Vector3 *v)
{
    //v->toString = malloc(1024);
    sprintf(v->toString, "(%d, %d, %d)", v->x, v->y, v->z);

    return v->toString;
}