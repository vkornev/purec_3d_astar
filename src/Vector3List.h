#ifndef VECTOR3_LIST_H
#define VECTOR3_LIST_H

#include "Vector3.h"

typedef struct Vector3List
{
    struct Vector3List *next;
    Vector3 *data;

    void (*Add)(struct Vector3List **, Vector3 *);
    void (*Remove)(struct Vector3List **, Vector3 *);
    struct Vector3List *(*GetItem)(struct Vector3List **, Vector3 *);
    int (*GetLength)(struct Vector3List **);
} Vector3List;

Vector3List *new_Vector3List(Vector3 *);
void delete_Vector3List(Vector3List **);

static void Vector3List_Add(Vector3List **, Vector3 *);
static void Vector3List_Remove(Vector3List **, Vector3 *);
static Vector3List *Vector3List_GetItem(Vector3List **, Vector3 *);
static int Vector3List_GetLength(struct Vector3List **);

#endif