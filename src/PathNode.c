#include "PathNode.h"
#include <stdlib.h>
#include <stdio.h>

PathNode *new_PathNode(Vector3 *v)
{
    PathNode *node = CreatePathNode();
    node->EstimateFullPathLength = &PathNode_EstimateFullPathLength;
    node->Equals = &PathNode_Equals;
    node->ToString = &PathNode_ToString;
    node->position = v;
    node->cameFrom = NULL;
    node->pathLengthFromStart = 0;
    node->heuristicEstimatePathLength = 0;

    return node;
}

void delete_PathNode(PathNode *node)
{
    if (node == NULL)
        return;

    delete_Vector3(node->position);
    free(node);
    node = NULL;
}

// Approximated full path length (F)
static float PathNode_EstimateFullPathLength(PathNode *node)
{
    return node->heuristicEstimatePathLength + node->pathLengthFromStart;
}

static bool PathNode_Equals(PathNode *a, PathNode *b)
{
    return a == b;
}

static char *PathNode_ToString(PathNode *node)
{
    char *str = malloc(64);

    Vector3 *nodePos = node->position;
    if (nodePos == NULL)
    {
        sprintf(str, "NULL");
        return NULL;
    }

    int vectLength = nodePos->GetStrLength(nodePos);
    char *posStr = malloc(vectLength * sizeof(*posStr));

    sprintf(str, "(%d, %d, %d)", node->position->x, node->position->y, node->position->z);

    char cameFromStr[22];
    int cameFromLength = 0;
    PathNode *cameFromNode = node->cameFrom;
    if (cameFromNode != NULL)
    {
        Vector3 *cameFromPos = cameFromNode->position;
        cameFromLength = 2 + cameFromPos->GetStrLength(cameFromPos);
        sprintf(cameFromStr, "<-(%d, %d, %d)", cameFromPos->x, cameFromPos->y, cameFromPos->z);
    }

    return str;
}