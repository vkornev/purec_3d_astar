#ifndef PATHNODE_H
#define PATHNODE_H

#include "Vector3.h"
#include <stdbool.h>

typedef struct PathNode
{
    // our position with the array
    Vector3 *position;
    // Previous node
    struct PathNode *cameFrom;
    // Length from the start (G)
    float pathLengthFromStart;
    // Approximated distance to the end (H)
    float heuristicEstimatePathLength;

    char toString[44];
    float (*EstimateFullPathLength)(struct PathNode *);
    bool (*Equals)(struct PathNode *, struct PathNode *);
    char *(*ToString)(struct PathNode *);
} PathNode;

#define CreatePathNode() malloc(sizeof(PathNode))

PathNode *new_PathNode(Vector3 *);
void delete_PathNode(PathNode *);

// Approximated full path length (F)
static float PathNode_EstimateFullPathLength(PathNode *);
static char *PathNode_ToString(PathNode *);
static bool PathNode_Equals(PathNode *a, PathNode *b);

#endif