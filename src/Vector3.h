#ifndef VECTOR3_H
#define VECTOR3_H

#include <stdbool.h>

typedef struct Vector3
{
    int x;
    int y;
    int z;
    char toString[24];
    int (*GetHashCode)(struct Vector3 *, int size);
    int (*GetStrLength)(struct Vector3 *);
    bool (*Equals)(struct Vector3 *, struct Vector3 *);
    char *(*ToString)(struct Vector3 *);
} Vector3;

Vector3 *new_Vector3(int, int, int);
void delete_Vector3(Vector3 *);

Vector3 *GetMinusOneVector3();
static bool Vector3_Equals(Vector3 *, Vector3 *b);
static int Vector3_GetHashCode(Vector3 *, int hashSize);
static int Vector3_GetStrLength(Vector3 *);
static char *Vector3_ToString(Vector3 *);

Vector3 *minusOneVector3;

#endif