#include "PathNodeList.h"
#include <stdlib.h>

PathNodeList *new_PathNodeList(PathNode *firstElement)
{
    PathNodeList *head = malloc(sizeof(*head));
    head->data = firstElement;
    head->next = NULL;
    head->Add = &PathNodeList_Add;
    head->Remove = &PathNodeList_Remove;
    head->GetItem = &PathNodeList_GetItem;
    head->GetLength = &PathNodeList_GetLength;
    return head;
}

void delete_PathNodeList(PathNodeList **head)
{
    if (*head == NULL)
        return;

    while (*head != NULL)
    {
        delete_PathNode((*head)->data);
        PathNodeList *current = *head;
        *head = (*head)->next;
        free(current);
    }
}

static void PathNodeList_Add(PathNodeList **head, PathNode *pn)
{
    //create new element
    PathNodeList *link = new_PathNodeList(pn);
    //assing top element of the list to new element's next link
    link->next = *head;
    //make new element the head
    *head = link;
}

static void PathNodeList_Remove(PathNodeList **head, PathNode *pn)
{
    if (*head == NULL)
        return;

    PathNodeList *current = *head;
    PathNodeList *previous = NULL;

    while (!current->data->Equals(current->data, pn))
    {
        if (current->next == NULL)
            return;

        //store reference to current link
        previous = current;
        //move to next link
        current = current->next;
    }

    //found a match, update the link
    if (current == *head)
        *head = (*head)->next;
    else
        previous->next = current->next;
}

static PathNodeList *PathNodeList_GetItem(PathNodeList **head, PathNode *pn)
{
    if (*head == NULL)
        return NULL;

    PathNodeList *current = *head;

    while (!current->data->Equals(current->data, pn))
    {
        //if it is last node
        if (current->next == NULL)
            return NULL;

        current = current->next;
    }

    return current;
}

static int PathNodeList_GetLength(PathNodeList **head)
{
    int length = 0;
    PathNodeList *current = *head;
    while (current != NULL)
    {
        length++;
        current = current->next;
    }

    return length;
}