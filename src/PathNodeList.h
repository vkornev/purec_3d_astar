#ifndef PATHNODE_LIST_H
#define PATHNODE_LIST_H

#include "PathNode.h"

typedef struct PathNodeList
{
    struct PathNodeList *next;
    PathNode *data;

    void (*Add)(struct PathNodeList **, PathNode *);
    void (*Remove)(struct PathNodeList **, PathNode *);
    struct PathNodeList *(*GetItem)(struct PathNodeList **, PathNode *);
    int (*GetLength)(struct PathNodeList **);
} PathNodeList;

PathNodeList *new_PathNodeList(PathNode *);
void delete_PathNodeList(PathNodeList **);

static void PathNodeList_Add(PathNodeList **, PathNode *);
static void PathNodeList_Remove(PathNodeList **, PathNode *);
static PathNodeList *PathNodeList_GetItem(PathNodeList **, PathNode *);
static int PathNodeList_GetLength(PathNodeList **);

#endif