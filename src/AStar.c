#include "AStar.h"
#include <math.h>
#include <stdlib.h>
#include "PathNode.h"
#include "Vector3List.h"
#include "PathNodeList.h"

Vector3List *FindPath(DataItem **map, Vector3 *border, Vector3 *start, Vector3 *goal)
{
    PathNodeList *closedSet = NULL;
    PathNode *node = new_PathNode(start);
    node->heuristicEstimatePathLength = GetHeuristicPathLength(start, goal);
    PathNodeList *openSet = new_PathNodeList(node);
    openSet->Add(&openSet, node);

    while (openSet->GetLength(&openSet) > 0)
    {
        PathNode *currentNode = GetBestNode(openSet);
        Vector3 *currentNodePos = currentNode->position;
        if (currentNodePos->Equals(currentNodePos, goal))
            return GetPathForNode(currentNode);

        openSet->Remove(&openSet, currentNode);

        if (closedSet == NULL)
            closedSet = new_PathNodeList(currentNode);
        else
            closedSet->Add(&closedSet, currentNode);

        PathNodeList *neighbours = GetNeighbours(map, border, currentNode, goal);
        for (PathNodeList *pi = neighbours; pi != NULL; pi = pi->next)
        {
            bool skip = false;
            int closedSetCount = closedSet->GetLength(&closedSet);
            for (PathNodeList *pj = closedSet; pj != NULL; pj = pj->next)
            {
                PathNode *currentPN = pi->data;
                if (currentPN->Equals(currentPN, pj->data))
                {
                    skip = true;
                    break;
                }
            }

            if (skip)
                continue;

            PathNode *openNode = NULL;
            for (PathNodeList *pk = openSet; pk != NULL; pk = pk->next)
            {
                if (pk->data->Equals(pk->data, pi->data))
                {
                    openNode = pk->data;
                    break;
                }
            }

            if (openNode == NULL)
                openSet->Add(&openSet, pi->data);
            else if (openNode->pathLengthFromStart > pi->data->pathLengthFromStart)
            {
                openNode->cameFrom = currentNode;
                openNode->pathLengthFromStart = pi->data->pathLengthFromStart;
            }
        }
    }

    return NULL;
}

static float Distance(Vector3 *start, Vector3 *end)
{
    int vecty = end->x - start->x;
    int vectx = end->y - start->y;
    int vectz = end->z - start->z;
    return sqrtf(vectx * vectx + vecty * vecty + vectz * vectz);
}

static int GetHeuristicPathLength(Vector3 *start, Vector3 *goal)
{
    return abs(goal->x - start->x) + abs(goal->y - start->y) + abs(goal->z - start->z);
}

static PathNode *GetBestNode(PathNodeList *pathNodeList)
{
    PathNode *result = NULL;
    float f = __FLT_MAX__;
    for (PathNodeList *pi = pathNodeList; pi != NULL; pi = pi->next)
    {
        PathNode *node = pi->data;
        float estim = node->EstimateFullPathLength(node);
        if (estim < f)
        {
            f = estim;
            result = node;
        }
    }
    return result;
}

static Vector3List *GetPathForNode(PathNode *pathNode)
{
    if (pathNode == NULL)
        return NULL;

    Vector3List *result = NULL;
    PathNode *currentNode = pathNode;
    while (currentNode != NULL)
    {
        if (result == NULL)
            result = new_Vector3List(currentNode->position);
        else
            result->Add(&result, currentNode->position);

        currentNode = currentNode->cameFrom;
    }
    //result.Reverse();
    return result;
}

static PathNodeList *GetNeighbours(DataItem **map, Vector3 *border, PathNode *pathNode, Vector3 *goal)
{
    PathNodeList *result = NULL;
    Vector3 *neighborPoints[6];
    neighborPoints[0] = new_Vector3(pathNode->position->x + 1, pathNode->position->y, pathNode->position->z);
    neighborPoints[1] = new_Vector3(pathNode->position->x - 1, pathNode->position->y, pathNode->position->z);
    neighborPoints[2] = new_Vector3(pathNode->position->x, pathNode->position->y + 1, pathNode->position->z);
    neighborPoints[3] = new_Vector3(pathNode->position->x, pathNode->position->y - 1, pathNode->position->z);
    neighborPoints[4] = new_Vector3(pathNode->position->x, pathNode->position->y, pathNode->position->z + 1);
    neighborPoints[5] = new_Vector3(pathNode->position->x, pathNode->position->y, pathNode->position->z - 1);

    //process right/left/up/down/forward/back tiles
    for (int i = 0; i < 6; i++)
    {
        bool occupied = neighborPoints[i]->x < 0 || neighborPoints[i]->y < 0 || neighborPoints[i]->z < 0 ||
                        neighborPoints[i]->x >= border->x || neighborPoints[i]->y >= border->y || neighborPoints[i]->z >= border->z ||
                        Vector3Hash_GetItem(map, neighborPoints[i])->data != 0;

        if (occupied)
        {
            delete_Vector3(neighborPoints[i]);
            continue;
        }

        PathNode *freeNeighbor = new_PathNode(neighborPoints[i]);
        freeNeighbor->cameFrom = pathNode;
        freeNeighbor->pathLengthFromStart = pathNode->pathLengthFromStart + Distance(pathNode->position, neighborPoints[i]);
        freeNeighbor->heuristicEstimatePathLength = GetHeuristicPathLength(neighborPoints[i], goal);

        if (result == NULL)
            result = new_PathNodeList(freeNeighbor);
        else
            result->Add(&result, freeNeighbor);
    }

    return result;
}