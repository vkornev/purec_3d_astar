#include <stdio.h>
#include "src/Vector3.h"
#include "src/PathNode.h"
#include "src/Vector3Hash.h"
#include "src/AStar.h"
#include "src/PathNodeList.h"
#include "src/Vector3List.h"

void Init()
{
    printf("size = %d\n", sizeof(char *));
    printf("Vector3 size = %d\n", sizeof(Vector3));
    Vector3 *vect1 = new_Vector3(-340, 20, 10);
    Vector3 *vect2 = new_Vector3(-65000, -65000, -65000);
    printf("%s\n", vect1->ToString(vect1));
    printf("%s\n", vect2->ToString(vect2));
    printf("vect1 string length = %d\n", vect1->GetStrLength(vect1));
    printf("vect2 string length = %d\n", vect2->GetStrLength(vect2));
    printf("Equal? %d\n", vect2->Equals(vect2, vect1));

    PathNode *pNode1 = new_PathNode(vect1);
    PathNode *pNode2 = new_PathNode(vect2);
    pNode2->cameFrom = pNode1;
    printf("PathNode1 = %s\n", pNode1->ToString(pNode1));
    printf("PathNode2 = %s\n", pNode2->ToString(pNode2));

    PathNodeList *pnList = new_PathNodeList(pNode1);
    pnList->Add(&pnList, pNode2);

    printf("List length = %d\n", pnList->GetLength(&pnList));

    pnList->Remove(&pnList, pNode1);

    printf("List length = %d\n", pnList->GetLength(&pnList));

    delete_PathNode(pNode1);
    delete_PathNode(pNode2);
}

DataItem *hashArray[HASH_ARRAY_SIZE];
DataItem *hashTableItem;
Vector3 *mapPositions[10][10][10];

void HashTableTest()
{
    for (int x = 0; x < 10; x++)
        for (int y = 0; y < 10; y++)
            for (int z = 0; z < 10; z++)
            {
                mapPositions[x][y][z] = new_Vector3(x, y, z);
                Vector3Hash_Add(hashArray, mapPositions[x][y][z], 0); //empty position
            }

    Vector3Hash_GetItem(hashArray, mapPositions[1][1][1])->data = 1;

    Vector3List *path = FindPath(hashArray, new_Vector3(4, 5, 2), new_Vector3(0, 0, 0), new_Vector3(3, 4, 0));

    Vector3List *currentPathPoint = path;
    while (currentPathPoint != NULL)
    {
        printf("%s\n", currentPathPoint->data->ToString(currentPathPoint->data));
        currentPathPoint = currentPathPoint->next;
    }

    Vector3Hash_Print(hashArray);
    hashTableItem = Vector3Hash_GetItem(hashArray, mapPositions[1][1][1]);

    if (hashTableItem != NULL)
    {
        printf("Element found: %d\n", hashTableItem->data);
    }
    else
    {
        printf("Element not found\n");
    }

    Vector3Hash_Remove(hashArray, hashTableItem);
    hashTableItem = Vector3Hash_GetItem(hashArray, mapPositions[1][1][1]);

    if (hashTableItem != NULL)
    {
        printf("Element found: %d\n", hashTableItem->data);
    }
    else
    {
        printf("Element not found\n");
    }
}

int main(int afgc, char **argv)
{
    Init();
    HashTableTest();

    return 0;
}